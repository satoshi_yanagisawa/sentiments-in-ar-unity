﻿using UnityEngine;
using System.Collections;

#if UNITY_5_3 || UNITY_5_3_OR_NEWER
using UnityEngine.SceneManagement;
#endif
using OpenCVForUnity;
using OpenCVFaceTracker;
namespace FaceTrackerExample
{
    /// <summary>
    /// Texture2D face tracker example.
    /// </summary>
    public class Texture2DFaceTrackerExample : MonoBehaviour
    {
        public EmotionApi emotionApi;

        /// <summary>
        /// The tracker_model_json_filepath.
        /// </summary>
        private string tracker_model_json_filepath;

        /// <summary>
        /// The haarcascade_frontalface_alt_xml_filepath.
        /// </summary>
        private string haarcascade_frontalface_alt_xml_filepath;

        public RenderTexture renderTexture;
        Texture2D textureRender;
        Mat imgMat;

        // Use this for initialization
        void Start()
        {
            renderTexture.width = (int)Screen.width / 4;
            renderTexture.height = (int)Screen.height / 4;
            #if UNITY_WEBGL && !UNITY_EDITOR
            StartCoroutine(getFilePathCoroutine());
            #else
            tracker_model_json_filepath = Utils.getFilePath("tracker_model.json");
            haarcascade_frontalface_alt_xml_filepath = Utils.getFilePath("haarcascade_frontalface_alt.xml");

            StartCoroutine(SetAR());
            #endif
        }

        #if UNITY_WEBGL && !UNITY_EDITOR
        private IEnumerator getFilePathCoroutine()
        {
            var getFilePathAsync_0_Coroutine = StartCoroutine(Utils.getFilePathAsync("tracker_model.json", (result) => {
                tracker_model_json_filepath = result;
            }));
            var getFilePathAsync_1_Coroutine = StartCoroutine(Utils.getFilePathAsync("haarcascade_frontalface_alt.xml", (result) => {
                haarcascade_frontalface_alt_xml_filepath = result;
            }));
             
            yield return getFilePathAsync_0_Coroutine;
            yield return getFilePathAsync_1_Coroutine;

        }
        #endif

        private IEnumerator SetAR()
        {
            while (true)
            {   
                Run();
                yield return new WaitForSeconds(5f);
            }
        }

        private void Run()
        {
            //initialize FaceTracker
            FaceTracker faceTracker = new FaceTracker(tracker_model_json_filepath);
            //initialize FaceTrackerParams
            FaceTrackerParams faceTrackerParams = new FaceTrackerParams();

            if(textureRender == null){
              textureRender = new Texture2D (renderTexture.width, renderTexture.height, TextureFormat.RGBA32, false);
            }

            Utils.textureToTexture2D (renderTexture, textureRender);

            if(imgMat == null) {
              imgMat = new Mat (textureRender.height, textureRender.width, CvType.CV_8UC4);
            }

            Utils.texture2DToMat (textureRender, imgMat, true);

            CascadeClassifier cascade = new CascadeClassifier();
            cascade.load(haarcascade_frontalface_alt_xml_filepath);

            //convert image to greyscale
            Mat gray = new Mat();
            Imgproc.cvtColor(imgMat, gray, Imgproc.COLOR_RGBA2GRAY);

            MatOfRect faces = new MatOfRect();
        
            Imgproc.equalizeHist(gray, gray);
        
            cascade.detectMultiScale(gray, faces, 1.1f, 2, 0
//                                                         | Objdetect.CASCADE_FIND_BIGGEST_OBJECT
                | Objdetect.CASCADE_SCALE_IMAGE, new OpenCVForUnity.Size(gray.cols() * 0.05, gray.cols() * 0.05), new Size());
        
            Debug.Log("faces " + faces.dump());
            if(faces.dump() != "[]")
            {
                Debug.Log("Found face");
               emotionApi.Call(textureRender);
            }
            else
            {
                 Debug.Log("Lost face");
            }
        }
    }
}