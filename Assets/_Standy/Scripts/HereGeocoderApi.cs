﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using UnityEngine.Networking;
using System;
using UnityEngine.UI;
public class HereGeocoderApi : MonoBehaviour {
    const string url = "https://geocoder.api.here.com/6.2/geocode.json";
    public string appId = "";
	public string appCode = "";
    const string notReachable = "-1";
    const string netWorkError = "-2";

    public void Call(string search, Action<string> callback)
    {
        StartCoroutine(_Call(search, callback));
    }
    IEnumerator _Call(string search, Action<string> callback)
	{
        if(Application.internetReachability == NetworkReachability.NotReachable)
        {
            Debug.LogWarning("NetworkReachability NotReachable");
            callback(notReachable);
        }
        else
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(url);
            sb.Append("?searchtext=").Append(search);
            sb.Append("&app_id=").Append(appId);
            sb.Append("&app_code=").Append(appCode);
            sb.Append("&gen=").Append("9");
            Debug.Log (sb.ToString());
            UnityWebRequest www = UnityWebRequest.Get(sb.ToString());
            yield return www.SendWebRequest();
            if(www.isNetworkError)
            {
                Debug.LogWarning(www.error);
                callback(netWorkError);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
                callback(www.downloadHandler.text);
            }
        }
    }
}
