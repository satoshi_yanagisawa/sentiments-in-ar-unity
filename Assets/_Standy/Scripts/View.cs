﻿using UnityEngine;
using UnityEngine.UI;
public class View : MonoBehaviour
{
	[SerializeField] Button buttonVersion;
	public Button ButtonVersion
	{
		get { return buttonVersion; }
	}
	[SerializeField] Button buttonArrow;
	public Button ButtonArrow
	{
		get { return buttonArrow; }
	}
	[SerializeField] Button buttonFooterArrow;
	public Button ButtonFooterArrow
	{
		get { return buttonFooterArrow; }
	}
	[SerializeField] Animator animatorHeader;
	public Animator AnimatorHeader
	{
		get { return animatorHeader; }
	}
	[SerializeField] Animator animatorFooter;
	public Animator AnimatorFooter
	{
		get { return animatorFooter; }
	}
	[SerializeField] InputField inputField;
	public InputField InputField
	{
		get { return inputField; }
	}
	[SerializeField] WebView webView;
	public WebView WebView
	{
		get { return webView; }
	}
    [SerializeField] RawImage rawImage;
	public RawImage RawImage
	{
		get { return rawImage; }
	}



	[SerializeField] Transform transformDebug;
	public Transform TransformDebug
	{
		get { return transformDebug; }
	}


	[SerializeField] Transform prefabAnger;
	public Transform PrefabAnger
	{
		get { return prefabAnger; }
	}
	[SerializeField] Transform prefabSurprise;
	public Transform PrefabSurprise
	{
		get { return prefabSurprise; }
	}
	[SerializeField] Transform prefabHappiness;
	public Transform PrefabHappiness
	{
		get { return prefabHappiness; }
	}
	[SerializeField] Transform prefabNeutral;
	public Transform PrefabNeutral
	{
		get { return prefabNeutral; }
	}
	[SerializeField] Transform prefabSadness;
	public Transform PrefabSadness
	{
		get { return prefabSadness; }
	}
	[SerializeField] Transform prefabDisgust;
	public Transform PrefabDisgust
	{
		get { return prefabDisgust; }
	}
	[SerializeField] Transform prefabFear;
	public Transform PrefabFear
	{
		get { return prefabFear; }
	}
	[SerializeField] Transform prefabContempt;
	public Transform PrefabContempt
	{
		get { return prefabContempt; }
	}
	[SerializeField] Transform prefabTwitter;
	public Transform PrefabTwitter
	{
		get { return prefabTwitter; }
	}
	[SerializeField] Transform prefabDestination;
	public Transform PrefabDestination
	{
		get { return prefabDestination; }
	}

	[SerializeField] Transform transformAnger;
	public Transform TransformAnger
	{
		get { return transformAnger; }
	}
	[SerializeField] Transform transformSurprise;
	public Transform TransformSurprise
	{
		get { return transformSurprise; }
	}
	[SerializeField] Transform transformHappiness;
	public Transform TransformHappiness
	{
		get { return transformHappiness; }
	}
	[SerializeField] Transform transformNeutral;
	public Transform TransformNeutral
	{
		get { return transformNeutral; }
	}
	[SerializeField] Transform transformDestination;
	public Transform TransformDestination
	{
		get { return transformDestination; }
	}
	[SerializeField] Transform transformSadness;
	public Transform TransformSadness
	{
		get { return transformSadness; }
	}
	[SerializeField] Transform transformTwitter;
	public Transform TransformTwitter
	{
		get { return transformTwitter; }
	}
	[SerializeField] Transform transformDisgust;
	public Transform TransformDisgust
	{
		get { return transformDisgust; }
	}
	[SerializeField] Transform transformFear;
	public Transform TransformFear
	{
		get { return transformFear; }
	}
	[SerializeField] Transform transformContempt;
	public Transform TransformContempt
	{
		get { return transformContempt; }
	}
	[SerializeField] Toggle toggleAnger;
	public Toggle ToggleAnger
	{
		get { return toggleAnger; }
	}
	[SerializeField] Toggle toggleSurprise;
	public Toggle ToggleSurprise
	{
		get { return toggleSurprise; }
	}
	[SerializeField] Toggle toggleHappiness;
	public Toggle ToggleHappiness
	{
		get { return toggleHappiness; }
	}
	[SerializeField] Toggle toggleNeutral;
	public Toggle ToggleNeutral
	{
		get { return toggleNeutral; }
	}
	[SerializeField] Toggle toggleDestination;
	public Toggle ToggleDestination
	{
		get { return toggleDestination; }
	}
	[SerializeField] Toggle toggleSadness;
	public Toggle ToggleSadness
	{
		get { return toggleSadness; }
	}
	[SerializeField] Toggle toggleTwitter;
	public Toggle ToggleTwitter
	{
		get { return toggleTwitter; }
	}
	[SerializeField] Toggle toggleDisgust;
	public Toggle ToggleDisgust
	{
		get { return toggleDisgust; }
	}
	[SerializeField] Toggle toggleFear;
	public Toggle ToggleFear
	{
		get { return toggleFear; }
	}
	[SerializeField] Toggle toggleContempt;
	public Toggle ToggleContempt
	{
		get { return toggleContempt; }
	}
}
