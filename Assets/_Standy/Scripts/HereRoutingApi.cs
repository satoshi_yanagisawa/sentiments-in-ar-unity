﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using UnityEngine.Networking;
using System;
public class HereRoutingApi : MonoBehaviour {
    const string url = "https://route.api.here.com/routing/7.2/calculateroute.json";
    public string appId = "";
	public string appCode = "";
    const string notReachable = "-1";
    const string netWorkError = "-2";

    public void Call(string current_lat, string current_lon, string dest_lat, string dest_lon, Action<string> callback)
    {
        StartCoroutine(_Call(current_lat, current_lon, dest_lat, dest_lon, callback));
    }
    IEnumerator _Call(string current_lat, string current_lon, string dest_lat, string dest_lon, Action<string> callback)
	{
        if(Application.internetReachability == NetworkReachability.NotReachable)
        {
            Debug.LogWarning("NetworkReachability NotReachable");
            callback(notReachable);
        }
        else
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(url);
            sb.Append("?mode=").Append("fastest;pedestrian");
            sb.Append("&waypoint0=").Append(current_lat + "," + current_lon);
            sb.Append("&waypoint1=").Append(dest_lat + "," + dest_lon);
            sb.Append("&app_id=").Append(appId);
            sb.Append("&app_code=").Append(appCode);
            Debug.Log (sb.ToString());
            UnityWebRequest www = UnityWebRequest.Get(sb.ToString());
            yield return www.SendWebRequest();
            if(www.isNetworkError)
            {
                Debug.LogWarning(www.error);
                callback(netWorkError);
            }
            else
            {
                Debug.Log(www.downloadHandler.text);
                callback(www.downloadHandler.text);
            }
        }
    }
}