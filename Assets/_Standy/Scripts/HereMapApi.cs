﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using UnityEngine.Networking;
using System;
using UnityEngine.UI;
public class HereMapApi : MonoBehaviour {
    [SerializeField] Texture2D errorTexture;
    const string url = "https://image.maps.api.here.com/mia/1.6/mapview";
    public string appId = "";
	public string appCode = "";
    public void Call(string lat, string lon, Action<Texture2D> callback)
    {
        StartCoroutine(_Call(lat, lon, callback));
    }
    IEnumerator _Call(string lat, string lon, Action<Texture2D> callback)
	{
        if(Application.internetReachability == NetworkReachability.NotReachable)
        {
            Debug.LogWarning("NetworkReachability NotReachable");
            callback(errorTexture);
        }
        else
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(url);
            sb.Append("?c=").Append(lat).Append(",").Append(lon);
            sb.Append("&z=").Append("12");
            sb.Append("&w=").Append("512");
            sb.Append("&h=").Append("512");
            sb.Append("&f=").Append("1");
            sb.Append("&t=").Append("14");
            sb.Append("&app_id=").Append(appId);
            sb.Append("&app_code=").Append(appCode);
            UnityWebRequest www = UnityWebRequest.Get(sb.ToString());
            yield return www.SendWebRequest();
            if(www.isNetworkError)
            {
                Debug.LogWarning(www.error);
                callback(errorTexture);
            }
            else
            {
                Debug.Log(www.downloadHandler.data);
                byte[] bytes = www.downloadHandler.data;
                Texture2D texture = new Texture2D(512, 512);
                texture.LoadImage(bytes);
                callback(texture);
            }
        }
    }
}