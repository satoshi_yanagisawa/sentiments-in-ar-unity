﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using MiniJSON;
using System;
using System.Linq;
public class EmotionApi : MonoBehaviour
{
    private enum ELocation
    {
        WestUS,
        EastUS2,
        WestCentralUS,
        WestEurope,
        SoutheastAsia,
    }

    [System.Flags]
    private enum EFaceAttributes
    {
        age             = 1 << 0,
        gender          = 1 << 1,
        smile           = 1 << 2,
        facialHair      = 1 << 3,
        headPose        = 1 << 4,
        glasses         = 1 << 5,
        emotion         = 1 << 6,
        hair            = 1 << 7,
        makeup          = 1 << 8,
        accessories     = 1 << 9,
        occlusion       = 1 << 10,
        blur            = 1 << 11,
        exposure        = 1 << 12,
        noise           = 1 << 13,
    }

    private enum EPostType
    {
        Binary,
        URL,
    }


    [SerializeField]
    private string      m_subscriptionKey   = string.Empty;
    [SerializeField]
    private ELocation   m_location          = default( ELocation );
    [SerializeField]
    private bool        m_requestFaceId     = true;
    [SerializeField]
    private bool        m_requestFaceLandmarks      = false;
    [SerializeField, EnumFlags]
    private EFaceAttributes m_requestFaceAttributes = 0;
    [SerializeField]
    private EPostType   m_postType      = default( EPostType );
    [SerializeField]
    private Texture2D   m_texture       = null;
    [SerializeField]
    private string      m_textureURL    = null;
    public Presenter presenter;

    public void Call(Texture2D texture2D)
    {
        m_texture = texture2D;
        StartCoroutine(Test());
    }

    IEnumerator Test(){
        string url      = GetFaceAPIURL();
        Dictionary<string, string> headers  = null;
        byte[] postData = null;

        if( m_postType == EPostType.Binary )
        {
            headers     = GetBinaryTypeHeader( m_subscriptionKey );
            postData    = GetTextureBinary( m_texture );
        }
        else
        {
            headers     = GetURTypeLHeader( m_subscriptionKey );
            postData    = GetTextureURLBinary( m_textureURL );
        }

        Debug.AssertFormat( postData != null && postData.Length > 0, "Postデータが不正です。" );

        string receivedJson = null;
        using( WWW www = new WWW( url, postData, headers ) )
        {
            yield return www;
            receivedJson    = www.text;
        }

        FaceData faceData = ConvertJson( receivedJson );
        Debug.Log( JsonUtility.ToJson( faceData ) );
		IDictionary jsonData = (IDictionary)Json.Deserialize(JsonUtility.ToJson( faceData ));
		IList persons = (IList)jsonData["persons"];
		foreach(IDictionary person in persons){
			IDictionary faceAttributes = (IDictionary)person["faceAttributes"];
            IDictionary emotion = (IDictionary)faceAttributes["emotion"];

            var myTable = new Dictionary<string, float>();
            myTable.Add("anger", Convert.ToSingle(emotion["anger"]));
            myTable.Add("contempt", Convert.ToSingle(emotion["contempt"]));
            myTable.Add("disgust", Convert.ToSingle(emotion["disgust"]));
            myTable.Add("fear", Convert.ToSingle(emotion["fear"]));
            myTable.Add("happiness", Convert.ToSingle(emotion["happiness"]));
            myTable.Add("neutral", Convert.ToSingle(emotion["neutral"]));
            myTable.Add("sadness", Convert.ToSingle(emotion["sadness"]));
            myTable.Add("surprise", Convert.ToSingle(emotion["surprise"]));
            var max = myTable.FirstOrDefault(x => x.Value.Equals(myTable.Values.Max()));
            presenter.SetEmotion(max.Key, max.Value);
            Debug.Log(max.Key);
            Debug.Log(max.Value);
		}
    }

    private string GetFaceAPIURL()
    {
        string localURL         = GetLocationURL( m_location );
        string idField          = GetFaceIdField( m_requestFaceId );
        string landmarksField   = GetFaceLandmarksField( m_requestFaceLandmarks );
        string attributesField  = GetFaceAttributesField( m_requestFaceAttributes );

        Debug.AssertFormat( !string.IsNullOrEmpty( localURL ), "不正な地域設定です。m_location={0}", m_location );

        return string.Format( "https://{0}.api.cognitive.microsoft.com/face/v1.0/detect{1}{2}{3}", localURL, idField, landmarksField, attributesField );
    }

    private string GetLocationURL( ELocation i_location )
    {
        return i_location.ToString().ToLower();
    }

    private string GetFaceIdField( bool i_flag )
    {
        return string.Format( "?returnFaceId={0}", i_flag );
    }

    private string GetFaceLandmarksField( bool i_flag )
    {
        return string.Format( "&returnFaceLandmarks={0}", i_flag );
    }

    private string GetFaceAttributesField( EFaceAttributes i_attributes )
    {
        string attributesStr    = string.Empty;
        foreach( EFaceAttributes attribute in System.Enum.GetValues( typeof( EFaceAttributes ) ) )
        {
            if( ( i_attributes & attribute ) != 0 )
            {
                attributesStr  += string.Format( "{0},", attribute );
            }
        }
        
        if( string.IsNullOrEmpty( attributesStr ) )
        {
            return string.Empty;
        }

        // 最後の","はいらないので削除(もっといい方法があるような気がするけど……)。
        attributesStr = attributesStr.Remove( attributesStr.Length - 1 );

        return string.Format( "&returnFaceAttributes={0}", attributesStr );
    }


    private Dictionary<string, string> GetBinaryTypeHeader( string i_key )
    {
        var header = new Dictionary<string, string>()
        {
            { "Content-Type",               "application/octet-stream" },
            { "Ocp-Apim-Subscription-Key",  i_key },
        };

        return header;
    }

    private Dictionary<string, string> GetURTypeLHeader( string i_key )
    {
        var header = new Dictionary<string, string>()
        {
            { "Content-Type",               "application/json; charset=UTF-8" },
            { "Ocp-Apim-Subscription-Key",  i_key },
        };

        return header;
    }

    private byte[] GetTextureBinary( Texture2D i_texture )
    {
        if( i_texture == null )
        {
            return null;
        }

        return i_texture.EncodeToPNG();
    }

    private byte[] GetTextureURLBinary( string i_url )
    {
        if( string.IsNullOrEmpty( i_url ) )
        {
            return null;
        }

        string json = string.Format( "{{ \"url\":\"{0}\" }}", i_url );
        return System.Text.Encoding.UTF8.GetBytes( json );
    }


    private FaceData ConvertJson( string i_json )
    {
        Debug.AssertFormat( !string.IsNullOrEmpty( i_json ), "Json情報が設定されていません。" );

        // Jsonデータがエラーの場合(もっといいエラーの判定があればいいんだけど……)。
        if( i_json.IndexOf( "error" ) > 0 )
        {
            FaceError faceError = null;
            try
            {
                faceError   = JsonUtility.FromJson<FaceError>( i_json );
            }
            catch( System.Exception )
            {
                faceError   = new FaceError();
            }

            Debug.LogWarningFormat( "顔情報の取得に失敗しているJson情報です。code={0}, message={1}", faceError.error.code, faceError.error.message );

            return null;            
        }

        // 無理やりJsonUtilityで使える形に変更する。
        string json = string.Format( "{{\"persons\":{0}}}", i_json );

        FaceData faceData   = null;

        try
        {
            faceData    = JsonUtility.FromJson<FaceData>( json );
        }
        catch( System.Exception i_exception )
        {
            Debug.LogWarningFormat( "Json情報をクラス情報へ変換することに失敗しました。exception={0}", i_exception );
            faceData    = null;
        }

        return faceData;
    }

} // class TestFaceAPI