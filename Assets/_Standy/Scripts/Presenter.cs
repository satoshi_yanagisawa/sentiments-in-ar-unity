﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using MiniJSON;
using System;
using System.Collections.Generic;
using GeoCoordinatePortable;
public class Presenter : MonoBehaviour {
	[SerializeField] View view;
    [SerializeField] LocationUpdater updater;
	[SerializeField] HereMapApi hereMapApi;
	[SerializeField] HereGeocoderApi hereGeocoderApi;
	[SerializeField] SwaggerApi swaggerApi;
	[SerializeField] HereRoutingApi hereRoutingApi;
    IEnumerator Start()
    {
		view.ButtonArrow.onClick.AddListener(OnClickButtonArrow);
		view.ButtonFooterArrow.onClick.AddListener(OnClickButtonFooterArrow);
		view.ButtonVersion.onClick.AddListener(OnClickButtonVersion);
		view.InputField.onEndEdit.AddListener(delegate(string text) {
			ValidateText(text);
        });
        view.ToggleAnger.onValueChanged.AddListener(delegate {
            view.TransformAnger.gameObject.SetActive(view.ToggleAnger.isOn);
        });
        view.ToggleSurprise.onValueChanged.AddListener(delegate {
            view.TransformSurprise.gameObject.SetActive(view.ToggleSurprise.isOn);
        });
        view.ToggleHappiness.onValueChanged.AddListener(delegate {
            view.TransformHappiness.gameObject.SetActive(view.ToggleHappiness.isOn);
        });
        view.ToggleNeutral.onValueChanged.AddListener(delegate {
            view.TransformNeutral.gameObject.SetActive(view.ToggleNeutral.isOn);
        });
        view.ToggleDestination.onValueChanged.AddListener(delegate {
            view.TransformDestination.gameObject.SetActive(view.ToggleDestination.isOn);
        });
        view.ToggleSadness.onValueChanged.AddListener(delegate {
            view.TransformSadness.gameObject.SetActive(view.ToggleSadness.isOn);
        });
        view.ToggleTwitter.onValueChanged.AddListener(delegate {
            view.TransformTwitter.gameObject.SetActive(view.ToggleTwitter.isOn);
        });
        view.ToggleDisgust.onValueChanged.AddListener(delegate {
            view.TransformDisgust.gameObject.SetActive(view.ToggleDisgust.isOn);
        });
        view.ToggleFear.onValueChanged.AddListener(delegate {
            view.TransformFear.gameObject.SetActive(view.ToggleFear.isOn);
        });
        view.ToggleContempt.onValueChanged.AddListener(delegate {
            view.TransformContempt.gameObject.SetActive(view.ToggleContempt.isOn);
        });
        while(!updater.CanGetLonLat())
            yield return null;
		swaggerApi.Call(updater.Latitude.ToString(), updater.Longitude.ToString(), "false", "1", (res) =>
		{
			IDictionary jsonData = (IDictionary)Json.Deserialize(res);
			if(jsonData == null) return;
			IDictionary _jsonData = (IDictionary)jsonData["results"];
			IList tweets = (IList)_jsonData["tweets"];
			foreach(IDictionary tweet in tweets)
			{
				float lat = Convert.ToSingle(tweet["lat_full"]);
				float lng = Convert.ToSingle(tweet["lng_full"]);
				Transform objSentiment = Instantiate(view.PrefabTwitter);
				objSentiment.SetParent(view.TransformTwitter);
				objSentiment.localPosition = Vector3.zero;
				var _distance = new GeoCoordinate(updater.Latitude, updater.Longitude).GetDistanceTo(new GeoCoordinate(lat, lng));
				Location _current = new Location(updater.Latitude, updater.Longitude);
				Location _destination = new Location(lat, lng);
				objSentiment.localRotation = Quaternion.Euler(0, 0, -(float)NaviMath.LatlngDirection(_current, _destination));
				Transform t = objSentiment.Find("SentimentBar");
				t.localPosition = new Vector3(0, (float)_distance, 0);
       			ParticleSystem.MainModule par = t.GetComponent<ParticleSystem>().main;
       			par.startSpeed = 10 * Convert.ToSingle(tweet["sentiment"]);
			}		
		});
		hereMapApi.Call(updater.Latitude.ToString(), updater.Longitude.ToString(), (res) =>
        {
            if(res != null)
                view.RawImage.texture = res;
		});
	}
	void OnClickButtonArrow()
	{
		view.AnimatorHeader.SetTrigger("OnClick");
	}
	bool isTouched;
	void OnClickButtonVersion()
	{
		view.TransformDebug.gameObject.SetActive(isTouched);
		if(!isTouched)
		{
			isTouched = true;
		} else {
			isTouched = false;
		}
	}
	void OnClickButtonFooterArrow()
	{
		view.AnimatorFooter.SetTrigger("OnClick");
	}
	void ValidateText(string text)
	{
		Debug.Log(text);
		if(text.ToLower() == "taipei")
		{
			text = "https://sky-gazer3.glitch.me/";
			if(RegexUtils.IsUrl(text))
				StartCoroutine(LoadWebView(text));
		}
		else
		{
			GetDestinationPoint(text);
		}
	}
	IEnumerator LoadWebView(string url)
	{
		view.AnimatorHeader.SetTrigger("OnLoad");
		yield return new WaitForSeconds(1.5f);
		view.WebView.enabled = true;
		view.WebView.SetUrl(url);
		StartCoroutine(view.WebView.Load());
	}
	public void GetDestinationPoint(string str)
	{
		hereGeocoderApi.Call(str, (res) =>
        {
			if(res == "-1")
			{
				Debug.Log("Network error");
			}
			else if(res == "-2")
			{
				Debug.Log("Network error");
			}
			else
			{
				IDictionary jsonData = (IDictionary)Json.Deserialize(res);
				if(jsonData == null) return;
				IDictionary response = (IDictionary)jsonData["Response"];
				IList view = (IList)response["View"];
				foreach(IDictionary _view in view){
					IList result = (IList)_view["Result"];
					foreach(IDictionary _result in result){
						IDictionary location = (IDictionary)_result["Location"];
						IDictionary displayPosition = (IDictionary)location["DisplayPosition"];
						float lat = Convert.ToSingle(displayPosition["Latitude"]);
						float lon = Convert.ToSingle(displayPosition["Longitude"]);
						SetDestinationPoint(lat.ToString(), lon.ToString());
					}
				}
			}
		});	
	}
	void SetDestinationPoint(string dest_lat, string dest_lon)
	{
		hereRoutingApi.Call(updater.Latitude.ToString(), updater.Longitude.ToString(), dest_lat, dest_lon, (res) =>
		{
			IDictionary jsonData = (IDictionary)Json.Deserialize(res);
			if(jsonData == null) return;
			double distance;
			Location current;
			Location destination;
			if(!string.IsNullOrEmpty(jsonData["type"].ToString()))
			{
				// Success
				distance = new GeoCoordinate(updater.Latitude, updater.Longitude).GetDistanceTo(new GeoCoordinate(Convert.ToSingle(dest_lat), Convert.ToSingle(dest_lon)));
				current = new Location(updater.Latitude, updater.Longitude);
				destination = new Location(Convert.ToSingle(dest_lat), Convert.ToSingle(dest_lon));			
			}
			else
			{
				// Failure
				distance = new GeoCoordinate(updater.Latitude, updater.Longitude).GetDistanceTo(new GeoCoordinate(Convert.ToSingle(dest_lat), Convert.ToSingle(dest_lon)));
				current = new Location(updater.Latitude, updater.Longitude);
				destination = new Location(Convert.ToSingle(dest_lat), Convert.ToSingle(dest_lon));
			}
			view.PrefabDestination.gameObject.SetActive(true);
			view.PrefabDestination.localPosition = new Vector3(0, 0, 100);
			view.PrefabDestination.localRotation = Quaternion.Euler(0, 0, -(float)NaviMath.LatlngDirection(current, destination));
			view.PrefabDestination.Find("SentimentBar").localPosition = new Vector3(0, (float)distance, 0);
		});					
	}
	public void SetEmotion(string key, float value)
	{
		Transform objSentiment;
		if(key == "anger"){
			objSentiment = Instantiate(view.PrefabAnger);
			objSentiment.SetParent(view.TransformAnger);
		} else if (key == "contempt"){
			objSentiment = Instantiate(view.PrefabContempt);
			objSentiment.SetParent(view.TransformContempt);
		} else if (key == "disgust"){
			objSentiment = Instantiate(view.PrefabDisgust);
			objSentiment.SetParent(view.TransformDisgust);
		} else if (key == "fear"){
			objSentiment = Instantiate(view.PrefabFear);
			objSentiment.SetParent(view.TransformFear);
		} else if (key == "happiness"){
			objSentiment = Instantiate(view.PrefabHappiness);
			objSentiment.SetParent(view.TransformHappiness);
		} else if (key == "neutral"){
			objSentiment = Instantiate(view.PrefabNeutral);
			objSentiment.SetParent(view.TransformNeutral);
		} else if (key == "sadness"){
			objSentiment = Instantiate(view.PrefabSadness);
			objSentiment.SetParent(view.TransformSadness);
		} else {
			objSentiment = Instantiate(view.PrefabSurprise);
			objSentiment.SetParent(view.TransformSurprise);
		}
		objSentiment.localPosition = Vector3.zero;
		double _distance = new GeoCoordinate(updater.FirstLatitude, updater.FirstLongitude).GetDistanceTo(new GeoCoordinate(updater.Latitude, updater.Longitude));
		Location _current = new Location(updater.FirstLatitude, updater.FirstLongitude);
		Location _destination = new Location(updater.Latitude, updater.Longitude);
		objSentiment.localRotation = Quaternion.Euler(0, 0, -(float)NaviMath.LatlngDirection(_current, _destination));
		Transform t = objSentiment.Find("SentimentBar");
		int d = (int)Math.Round((float)_distance);
		#if UNITY_EDITOR
			t.localPosition = new Vector3(0, 0, 0);
		#else
			t.localPosition = new Vector3(0, (float)_distance, 0);
		#endif
       	ParticleSystem.MainModule par = t.GetComponent<ParticleSystem>().main;
		par.startSpeed = 10 * value;
	}
}