﻿using UnityEngine;
using System.Collections;

public class LocationUpdater : MonoBehaviour
{    
    /// <summary>経緯度取得間隔（秒）</summary>
    private const float IntervalSeconds = 1.0f;

    /// <summary>ロケーションサービスのステータス</summary>
    public LocationServiceStatus locationServiceStatus;

    /// <summary>経度</summary>
    public float Longitude { get; private set; } = 139.701636f;

    /// <summary>経度</summary>
    public float Latitude { get; private set; } = 35.658034f;
    /// <summary>経度</summary>
    public float FirstLongitude { get; private set; }

    /// <summary>経度</summary>
    public float FirstLatitude { get; private set; }
    bool init;
    public Transform transformPivot;
    public Transform transformCamera;
    Vector3 targetPos;

    /// <summary>緯度経度情報が取得可能か</summary>
    /// <returns>可能ならtrue、不可能ならfalse</returns>
    public bool CanGetLonLat()
    {
        #if UNITY_EDITOR
            return true;
        #else
            return Input.location.isEnabledByUser;
        #endif
    }

    void Update()
    {
        if(targetPos == null) return;
        transformPivot.localEulerAngles = Vector3.Lerp( transformPivot.localEulerAngles, targetPos, 1f );
    }
    /// <summary>経緯度取得処理</summary>
    /// <returns>一定期間毎に非同期実行するための戻り値</returns>
    private IEnumerator Start()
    {
        Input.compass.enabled = true;
        Debug.Log (string.Format("<b>精度</b>：{0}", Input.compass.headingAccuracy));
        Debug.Log (string.Format("<b>タイムスタンプ</b>：{0}", Input.compass.timestamp));
        while (true)
        {
            locationServiceStatus = Input.location.status;
            if (Input.location.isEnabledByUser)
            {
                switch (locationServiceStatus)
                {
                    case LocationServiceStatus.Stopped:
                        Input.location.Start();
                        break;
                    case LocationServiceStatus.Running:
                        if(!init)
                        {
                            FirstLongitude = Input.location.lastData.longitude;
                            FirstLatitude = Input.location.lastData.latitude;
                            init = true;
                        }
                        Longitude = Input.location.lastData.longitude;
                        Latitude = Input.location.lastData.latitude;
                        break;
                    default:
                        break;
                }
            }
            // Transformの向きを真北にする
            if(transformCamera.localEulerAngles.x == 0 && transformCamera.localEulerAngles.y == 0 && transformCamera.localEulerAngles.z == 0)
            {
                transformPivot.localEulerAngles = new Vector3(0, 0, 0);
            }
            else
            {
                targetPos = new Vector3(0, 0, Input.compass.magneticHeading - transformCamera.localEulerAngles.y);
            }
                Debug.Log (
                    string.Format ("magneticHeading : {0}, rawVector : {1}, trueHeading : {2}",
                        Input.compass.magneticHeading,
                        Input.compass.rawVector,
                        Input.compass.trueHeading
                    )
                );
                Debug.Log (
                    string.Format ("x : {0}, y : {1}, z : {2}",
                        transformPivot.localEulerAngles.x,
                        transformPivot.localEulerAngles.y,
                        transformPivot.localEulerAngles.z
                    )
                );
            yield return new WaitForSeconds(IntervalSeconds);
        }
    }
}