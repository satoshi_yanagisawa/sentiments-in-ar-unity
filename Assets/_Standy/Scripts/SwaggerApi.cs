﻿using UnityEngine;
using System.Collections;
using UnityEngine.Networking;
using System.Text;
using System;
using System.Collections.Generic;
public class SwaggerApi: MonoBehaviour
{
    const string url = "http://35.189.128.32/v1/tweets/location/";
    const string notReachable = "-1";
    const string netWorkError = "-2";
    public void Call(string lat, string lng, string disaster, string radius, Action<string> callback)
    {
        StartCoroutine(_Call(lat, lng, disaster, radius, callback));
    }
    IEnumerator _Call(string lat, string lng, string disaster, string radius, Action<string> callback)
	{
        if(Application.internetReachability == NetworkReachability.NotReachable)
        {
            Debug.LogWarning("NetworkReachability NotReachable");
            callback (notReachable);
        }
        else
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(url);
            sb.Append("?lat=");
            sb.Append(lat);
            sb.Append("&lng=");
            sb.Append(lng);
            sb.Append("&disaster=");
            sb.Append(disaster);
            sb.Append("&radius=");
            sb.Append(radius);
            UnityWebRequest www = UnityWebRequest.Get(sb.ToString());
            yield return www.SendWebRequest();
            if(www.isNetworkError)
            {
                Debug.LogWarning(www.error);
                callback(netWorkError);
            }
            else {
                Debug.Log(www.downloadHandler.text);
                callback(www.downloadHandler.text);
            }
        }
    }
}